from django.db import models

# Create your models here.

"""
Crear un nuevo proyecto Django, con una app llamada VUELOS, que sirva para manejar los diferentes vuelos que puede ofertar una determinada compañía aérea. Estas son las entidades que maneja:

Aeropuerto(nombre, ciudad, siglas)
Vuelo(aeropuerto_salida, fecha_salida, aeropuerto_llegada, fecha_llegada, codigo_vuelo)
Cliente(nombre,apellidos,email, fecha_nacimiento)
Reserva(vuelo, cliente, fecha_reserva, precio)

Algunas notas:
Un vuelo sale de un aeropuerto y llega a otro aeropuerto.
Las fechas de salida y llegada representan fecha y hora.
El código de vuelo no tiene que ser único a lo largo del tiempo
"""

class Aeropuerto(models.Model):
    nombre = models.CharField('Nombre', max_length=200)
    ciudad = models.CharField('Ciudad', max_length=500)
    siglas = models.CharField('Siglas', max_length=100)

    def __str__(self):
        return self.nombre+', ('+self.siglas+')'

class Vuelo(models.Model):
    aeropuerto_salida = models.ForeignKey(Aeropuerto, on_delete=models.SET, related_name = 'aeropuerto_salida')
    fecha_salida = models.DateTimeField('Fecha de salida', null=False, blank=False)
    aeropuerto_llegada = models.ForeignKey(Aeropuerto, on_delete = models.SET, null=False, blank = True, related_name = 'aeropuerto_llegada')
    fecha_llegada = models.DateTimeField('Fecha de llegada', null=False, blank=False)
    codigo_vuelo = models.IntegerField('Código', unique=False)

    def __str__(self):
        return 'Salida: '+str(self.fecha_salida)+', llegada: '+str(self.fecha_llegada)

class Cliente(models.Model):
    nombre = models.CharField('Nombre', max_length=200)
    apellidos = models.CharField('Apellidos', max_length=500)
    email = models.EmailField('Email', max_length=500)
    fecha_nacimiento = models.DateField('Fecha Nacimiento')

    def __str__(self):
        return self.nombre+' '+self.apellidos

class Reserva(models.Model):
    vuelo = models.ForeignKey(Vuelo, on_delete=models.SET)
    cliente = models.ForeignKey(Cliente, on_delete=models.SET)
    fecha_reserva = models.DateTimeField('Fecha reserva')
    precio = models.DecimalField('Precio', null=False, blank=False, decimal_places=2, max_digits=5)

    def __str__(self):
        return str(self.precio)+'€'
    