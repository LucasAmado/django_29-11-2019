from django.contrib import admin

# Register your models here.

from .models import Aeropuerto, Vuelo, Cliente, Reserva

@admin.register(Aeropuerto)
class AeropuertoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'ciudad', 'siglas')
    list_filter = ('nombre', 'ciudad', 'siglas')

@admin.register(Vuelo)
class VueloAdmin(admin.ModelAdmin):
    list_display = ('aeropuerto_salida', 'fecha_salida', 'aeropuerto_llegada', 'fecha_llegada', 'codigo_vuelo')
    list_filter = ('aeropuerto_salida', 'fecha_salida', 'aeropuerto_llegada', 'fecha_llegada')

@admin.register(Cliente)
class ClienteAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellidos', 'email', 'fecha_nacimiento')
    list_filter = ('apellidos', 'email', 'fecha_nacimiento')

@admin.register(Reserva)
class ReservaAdmin(admin.ModelAdmin):
    list_display = ('vuelo', 'cliente', 'fecha_reserva', 'precio')
    list_filter = ('vuelo', 'cliente', 'fecha_reserva', 'precio')