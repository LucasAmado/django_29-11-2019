# Generated by Django 2.2.7 on 2019-11-29 00:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Aeropuerto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200, verbose_name='Nombre')),
                ('ciudad', models.CharField(max_length=500, verbose_name='Ciudad')),
                ('siglas', models.CharField(max_length=100, verbose_name='Siglas')),
            ],
        ),
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200, verbose_name='Nombre')),
                ('apellidos', models.CharField(max_length=500, verbose_name='Apellidos')),
                ('email', models.EmailField(max_length=500, verbose_name='Email')),
                ('fecha_nacimiento', models.DateField(verbose_name='Fecha Nacimiento')),
            ],
        ),
        migrations.CreateModel(
            name='Vuelo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_salida', models.DateTimeField(verbose_name='Fecha de salida')),
                ('fecha_llegada', models.DateTimeField(verbose_name='Fecha de llegada')),
                ('codigo_vuelo', models.IntegerField(verbose_name='Código')),
                ('aeropuerto_llegada', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.SET, related_name='aeropuerto_llegada', to='vuelos.Aeropuerto')),
                ('aeropuerto_salida', models.ForeignKey(on_delete=django.db.models.deletion.SET, related_name='aeropuerto_salida', to='vuelos.Aeropuerto')),
            ],
        ),
        migrations.CreateModel(
            name='Reserva',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_reserva', models.DateTimeField(verbose_name='Fecha reserva')),
                ('precio', models.DecimalField(decimal_places=2, max_digits=5, verbose_name='Precio')),
                ('cliente', models.ForeignKey(on_delete=django.db.models.deletion.SET, to='vuelos.Cliente')),
                ('vuelo', models.ForeignKey(on_delete=django.db.models.deletion.SET, to='vuelos.Vuelo')),
            ],
        ),
    ]
