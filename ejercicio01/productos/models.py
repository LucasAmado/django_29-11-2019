from django.db import models
from django.utils.timezone import now

# Create your models here.

"""Un producto pertenece a una y solo una categoría
Una categoría no tiene porqué tener una categoría padre. 
"""

class CategoriaPadre(models.Model):
    nombre = models.CharField('Nombre', max_length=100)

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']

class Categoria(models.Model):
    #nombre, categoria_padre
    nombre = models.CharField('Nombre', max_length=100)
    categoria_padre = models.ForeignKey('CategoriaPadre', on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']

class Producto(models.Model):
    #nombre, descripcion, url_imagen, precio_unidad, categoria
    nombre = models.CharField('Nombre', blank=False, max_length=120)
    descripcion = models.CharField('Descripción', max_length=200)
    url_imagen = models.ImageField('Imagen', max_length=200)
    precio_unidad = models.DecimalField('Precio unitario', decimal_places=2, max_digits=7)
    categoria = models.ForeignKey('Categoria', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.nombre+', '+str(self.precio_unidad)

    class Meta:
        ordering = ['nombre']