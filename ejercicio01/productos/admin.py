from django.contrib import admin

# Register your models here.

from .models import CategoriaPadre, Categoria, Producto

"""admin.site.register(CategoriaPadre)
admin.site.register(Categoria)
admin.site.register(Producto)"""

@admin.register(CategoriaPadre)
class CategoriaPadreAdmin(admin.ModelAdmin):
    pass

@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
    pass

@admin.register(Producto)
class ProductoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'precio_unidad', 'categoria')
    list_filter = ('nombre', 'precio_unidad', 'categoria')