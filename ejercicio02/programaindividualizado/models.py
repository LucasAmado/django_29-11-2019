from django.db import models

# Create your models here.

"""
Residente(nombre, apellidos, fecha_nacimiento)
Familiar(nombre, apellidos, fecha_nacimiento, parentesco)
Informe(fecha_informe, Partes[1..*])
ParteInforme(tipo, valoracion_inicial, objetivos, informe) 

Algunas notas:
El tipo de ParteInforme puede tomar alguno de estos valores: SANTIARIA, FUNCIONAL, PSIQUICA, SOCIAL, TERAPIA OCUPACIONAL.
Las fechas no tienen porqué almacenar la hora.
Un informe consta de muchas partes.
"""

class Residente(models.Model):
    #nombre, apellidos, fecha_nacimiento
    nombre = models.CharField('Nombre', null=False, blank=False, max_length=150)
    apellidos = models.CharField('Apellidos', null=False, blank=False, max_length=250)
    fecha_nacimiento = models.DateField('Fecha_nacimiento')

    def __str__(self):
        return self.nombre+' '+self.apellidos

class Familiar(models.Model):
    #nombre, apellidos, fecha_nacimiento, parentesco
    nombre = models.CharField('Nombre', null=False, blank=False, max_length=150)
    apellidos = models.CharField('Apellidos', null=False, blank=False, max_length=250)
    fecha_nacimiento = models.DateField('Fecha_nacimiento')
    parentesco = models.CharField('Parentesco', max_length=100)

    def __str__(self):
        return self.nombre+' '+self.apellidos

class Informe(models.Model):
    #fecha_informe, Partes[1..*]
    fecha_informe = models.DateField('Fecha informe', null=False, blank=False)

    def __str__(self):
        return self.fecha_informe

class ParteInforme(models.Model):
    #tipo, valoracion_inicial, objetivos, informe
    tipos = (
        ('San', 'Sanitaria'),
        ('Fun', 'Funcional'),
        ('Psi', 'Psiquica'),
        ('Soc', 'Social'),
        ('Toc', 'Terapia Ocupacional')
    )
    tipo = models.CharField('Tipo', choices = tipos, blank = False, max_length=3)
    valoracion_inicial = models.CharField('Valoración inicial', max_length=2000, blank=False)
    objetivos = models.CharField('Objetivos', blank=False, max_length=1500)
    informe = models.ForeignKey('Informe', related_name="partes", on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.tipo