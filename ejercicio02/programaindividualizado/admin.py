from django.contrib import admin
from .models import Residente, Familiar, Informe, ParteInforme

# Register your models here.

@admin.register(Residente)
class ResidenteAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellidos', 'fecha_nacimiento')
    list_filter = ('nombre', 'apellidos', 'fecha_nacimiento')

@admin.register(Familiar)
class FamiliarAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellidos', 'fecha_nacimiento', 'parentesco')
    list_filter = ('nombre', 'apellidos', 'fecha_nacimiento')

@admin.register(Informe)
class InformeAdmin(admin.ModelAdmin):
    pass

@admin.register(ParteInforme)
class ParteInformeAdmin(admin.ModelAdmin):
    list_display = ('tipo', 'valoracion_inicial', 'objetivos', 'informe')
    list_filter = ('tipo', 'informe')
